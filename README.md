# sneakchat

use [sneak](https://gitlab.com/Forthegy/sneak) to generate and share your keys

to run:

```
cargo run [your private sneak key name] [their public sneak key name] [display name] [host/connect] [ip address; default is 0.0.0.0:5524]
```

port forwarding is necessary for the host
