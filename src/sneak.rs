use std::{
    env,
    path::{
        Path,
        PathBuf,
    },
    fs::{
        self,
        File,
    },
    io,
    io::Write,
    convert::TryInto,
};
use home::home_dir;
use whoami::username;
pub use rand::{
    RngCore,
    rngs::OsRng,
};
pub use x25519_dalek::{
    StaticSecret,
    SharedSecret,
    PublicKey,
    EphemeralSecret,
};
use ascii85;
use chacha20::XChaCha20;
use cipher::{
    NewStreamCipher,
    consts::U24,
};
use chacha20poly1305::{
    XNonce,
    Key,
    XChaCha20Poly1305,
    aead::{
        self,
        Aead,
        NewAead,
    },
};


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


pub type SneakKey = [u8; 32];
pub const XNONCE_SIZE: usize = 24;

pub fn generate_xnonce(csprng: &mut OsRng) -> [u8; XNONCE_SIZE]  {
    let mut nonce = [0u8; XNONCE_SIZE];
    csprng.fill_bytes(&mut nonce);
    nonce
}

pub fn encode(data: &[u8]) -> String {
    ascii85::encode(data).trim_start_matches("<~").trim_end_matches("~>").to_string()
}

// todo: add decoding errors
pub fn decode(data: &str) -> Vec<u8> {
    ascii85::decode(data).unwrap()
}

pub fn encrypt(csprng: &mut OsRng, key: &SneakKey, plaintext: &[u8]) -> Result<([u8; XNONCE_SIZE], Vec<u8>), aead::Error> {
    let cipher = XChaCha20Poly1305::new(Key::from_slice(&key[..]));
    let nonce = generate_xnonce(csprng);
    let ciphertext = cipher.encrypt(XNonce::from_slice(&nonce[..]), plaintext.as_ref())?;
    Ok((nonce, ciphertext))
}

pub fn decrypt(key: &SneakKey, nonce: [u8; XNONCE_SIZE], ciphertext: &[u8]) -> Result<Vec<u8>, aead::Error> {
    let cipher = XChaCha20Poly1305::new(Key::from_slice(&key[..]));
    let plaintext = cipher.decrypt(XNonce::from_slice(&nonce[..]), ciphertext.as_ref())?;
    Ok(plaintext)
}

pub enum ReadKeyError {
    Io { error: io::Error },
    NoHomeDir,
    NoFile,
    CannotRead,
}

pub fn get_system_username() -> String {
    let username = username();
    if username.is_empty() { "user".into() } else { username }
}

const SNEAK_FOLDER_NAME: &'static str = if cfg!(windows) { "sneakdata" } else { ".sneak" };

pub fn get_data_dir() -> Result<PathBuf, ()> {
    let mut data_dir = home_dir()
        .ok_or(())?;
    data_dir.push(SNEAK_FOLDER_NAME);
    Ok(data_dir)
}

pub fn get_key_path(key_name: &str, public: bool) -> Result<PathBuf, ()> {
    Ok(get_data_dir()
        .or(Err(()))?
        .join(if public { format!("{}.pub", key_name) } else { key_name.into() }))
}

pub fn read_key(key_name: &str, public: bool) -> Result<SneakKey, ReadKeyError> {
    let path = get_key_path(key_name, public)
        .or(Err(ReadKeyError::NoHomeDir))?;
    if !path.is_file() { return Err(ReadKeyError::NoFile); }
    let read_result = fs::read_to_string(path)
        .or(Err(ReadKeyError::CannotRead))?;
    let line = read_result.split('\n')
        .find(|s| !["", "===PUBLIC Curve25519===", "===END Curve25519==="].contains(&s.trim()))
        .ok_or(ReadKeyError::CannotRead)?;
    decode(line).as_slice().try_into()
        .or(Err(ReadKeyError::CannotRead))
}

pub fn generate_keypair(rng: &mut OsRng, key_name: &str) -> Result<(StaticSecret, PublicKey), ()> {
    let private = StaticSecret::new(*rng);
    let public = PublicKey::from(&private);
    write_key(key_name, false, private.to_bytes())
        .or(Err(()))?;
    write_key(key_name, true, public.to_bytes())
        .or(Err(()))?;
    Ok((private, public))
}

pub enum WriteKeyError {
    CannotCreateFolder,
    CannotCreateFile,
    CannotWriteFile,
}

pub fn write_key(key_name: &str, public: bool, key: SneakKey) -> Result<(), WriteKeyError> {
    let path = get_key_path(key_name, public)
        .or(Err(WriteKeyError::CannotCreateFolder))?;
    fs::create_dir_all(
        path.parent()
            .ok_or(WriteKeyError::CannotCreateFolder)?
        ).or(Err(WriteKeyError::CannotCreateFolder))?;
    let mut file = File::create(path)
        .or(Err(WriteKeyError::CannotCreateFile))?;
    file.write_all(encode(&key[..]).as_bytes())
        .or(Err(WriteKeyError::CannotWriteFile))?;
    Ok(())
}
