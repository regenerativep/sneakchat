use std::{
    thread,
    time::Duration,
    convert::TryInto,
    sync::{
        Arc,
        Mutex,
        mpsc::{
            Sender,
            Receiver,
            channel,
        },
    },
    io::{
        self,
//        stdin,
//        Stdin,
        stdout,
//        Read,
        Write,
//        Lines,
        BufReader,
        BufRead,
        BufWriter,
    },
    net::{
        TcpListener,
        TcpStream,
        Shutdown,
        SocketAddr,
        ToSocketAddrs
    },
};
mod sneak;
use sneak::{
    encrypt,
    decrypt,
//    generate_xnonce,
    encode,
    decode,
    SneakKey,
    SharedSecret,
    EphemeralSecret,
    StaticSecret,
    PublicKey,
    OsRng,
    read_key,
    ReadKeyError,
};
use crossterm::{
    ExecutableCommand,
//    Command,
//    execute,
    terminal::{
        self,
        ClearType,
    },
    cursor,
    event::{
        read,
        Event,
        KeyEvent,
        KeyCode,
//        KeyModifiers,
    },
};

struct UserMessage {
    name: String,
    msg: String,
}

impl UserMessage {
    fn to_string(&self) -> String {
        format!("[{}] {}", self.name, self.msg)
    }
}

enum OutputMessage {
    NormalOut { msg: String },
    UserMessage { data: UserMessage },
    UpdateInput { inp: String },
    UpdateInputCursor { pos: usize },
    UpdateOffset { amount: isize },
    Resize { w: u16, h: u16 },
    Shutdown,
}

fn main() {
   let mut args = std::env::args().fuse();
   let _path = args.next();
   let ps_private_name = match args.next() {
       Some(name) => name,
       None => return println!("you need to enter your name for the preshared private key"),
   };
   let ps_public_name = match args.next() {
       Some(name) => name,
       None => return println!("you need to enter the peer's name for the preshared public key"),
   };
   let keys = [(ps_private_name, false), (ps_public_name, true)].iter()
       .map(|(name, public)| {
           match read_key(name.as_str(), *public) {
               Ok(key) => Some(key),
               _ => {
                   println!("failed to read {} key", if *public { "public" } else { "private" });
                   None
               },
           }
       }).collect::<Vec<Option<SneakKey>>>();
   if let Some(_) = keys.iter().find(|k| match k { Some(_) => false, None => true, }) {
       return;
   }
   let preshared_private = keys[0].unwrap();
   let preshared_public = keys[1].unwrap();
   println!("read preshared keys");
   let name = match args.next() {
       Some(name) => name,
       None => return println!("you need to enter a name"),
   };
   enum Action { Host, Connect }
   let action = match args.next() {
       Some(action) => {
           match action.as_str() {
               "host" => Action::Host,
               "connect" => Action::Connect,
               _ => return println!("expected actions \"host\" or \"connect\""),
           }
       },
       _ => return println!("expected an action"),
   };
   let mut ip_str = args.next().unwrap_or("0.0.0.0".into());
   if let None = ip_str.chars().find(|&c| c == ':') {
       ip_str.push_str(":5524");
   }
   let ip = match ip_str.to_socket_addrs() {
       Ok(mut addrs_iter) => addrs_iter.next().unwrap(),
       Err(e) => return println!("error parsing address: {}", e),
   };
   let mut stream = match action {
       Action::Connect => match connect_client(ip) {
           Ok(stream) => stream,
           Err(e) => return println!("failed to connect: {}", e),
       },
       Action::Host => match connect_server(ip) {
           Ok(stream) => stream,
           Err(e) => return println!("failed to wait for connection: {}", e),
       },
   };
   println!("connected");
   let mut rng = OsRng;
   let shared_secret = match exchange(&mut stream, &mut rng, preshared_public, preshared_private) {
       Ok(secret) => secret,
       Err(e) => return println!("failed to exchange keys: {}", e),
   };
   let shared_secret = Arc::new(Mutex::new(shared_secret));
   println!("obtained shared secret key");

   let (out_sender, out_receiver) = channel::<OutputMessage>();


   // read/write machines for stream
   let stream = Arc::new(stream);
   let (writer_sender, writer_receiver) = channel::<Option<String>>();
   let writer_sender_cl = writer_sender.clone();
   // read
   let shared_secret_ref = Arc::clone(&shared_secret);
   let out_sender_cl = out_sender.clone();
   let stream_ref = Arc::clone(&stream);
   let read_stream_handle = thread::spawn(move || {
       run_read_stream(stream_ref, out_sender_cl, writer_sender_cl, shared_secret_ref)
   });

   let out_sender_cl = out_sender.clone();
   let shared_secret_ref = Arc::clone(&shared_secret);
   let stream_ref = Arc::clone(&stream);
   //write
   let write_stream_handle = thread::spawn(move || {
       run_write_stream(stream_ref, writer_receiver, out_sender_cl, shared_secret_ref, &mut rng)
   });

   println!("taking control of terminal");
   terminal::enable_raw_mode().unwrap();
   let stdout_thread_handle = thread::spawn(move || {
       stdout()
           .execute(terminal::EnterAlternateScreen).unwrap()
           .execute(terminal::DisableLineWrap).unwrap()
           .execute(terminal::Clear(ClearType::All)).unwrap();
       let res = run_stdout(out_receiver);
       stdout()
           .execute(terminal::EnableLineWrap).unwrap()
           .execute(terminal::LeaveAlternateScreen).unwrap();
       res
   });

   match run_stdin(out_sender, writer_sender, name) { _ => {}, }

   terminal::disable_raw_mode().unwrap();
   println!("");
   println!("waiting for stdout thread");
   match stdout_thread_handle.join() {
       Ok(res) => if let Err(res) = res {
           match res {
               RunStdoutError::StdoutError => println!("failed to use stdout"),
               RunStdoutError::CrossTermError { error: e } => println!("crossterm failed: {}", *e),
           }
       },
       Err(_) => println!("failed to join stdout thread"),
   };
   println!("waiting for read stream thread");
   match read_stream_handle.join() {
       Ok(res) => if let Err(res) = res {
           match res {
               RunReadStreamError::Conversion => println!("data conversion error"),
               _ => {},
           }
       },
       Err(_) => println!("failed to join read stream thread"),
   }
   println!("waiting for write stream thread");
   match write_stream_handle.join() {
       Ok(res) => if let Err(res) = res {
           match res {
               RunWriteStreamError::Encryption => println!("encryption error"),
               RunWriteStreamError::StreamWrite => println!("stream write error"),
               RunWriteStreamError::Output => println!("closed from output channel going down"),
               RunWriteStreamError::Shutdown { error } => println!("io error shutting down stream: {}", *error),
           }
       },
       Err(_) => println!("failed to join write stream thread"),
   }
}

enum RunStdinError {
    OutputError,
    WriteError,
}

fn run_stdin(out_sender: Sender<OutputMessage>, writer_sender: Sender<Option<String>>, mut name: String) -> Result<(), RunStdinError> {
   let mut input_buffer: Vec<char> = vec!();
   fn input_buffer_to_string(buff: &Vec<char>) -> String {
       let mut out_str = String::new();
       for c in buff.iter() {
           out_str.push(*c);
       }
       out_str
   }
   let mut input_buffer_cursor = 0;
   while let Ok(event) = read() {
       if let Event::Key(KeyEvent { code, modifiers: _ }) = event {
           let mut made_change = false;
           let mut moved_cursor = false;
           match code {
               KeyCode::Backspace => {
                   if input_buffer_cursor >= 1 && input_buffer_cursor <= input_buffer.len() {
                       input_buffer.remove(input_buffer_cursor - 1);
                       input_buffer_cursor -= 1;
                       made_change = true;
                   }
               },
               KeyCode::Delete => {
                   if input_buffer_cursor < input_buffer.len() {
                       input_buffer.remove(input_buffer_cursor);
                       made_change = true;
                   }
               },
               KeyCode::Char(c) => {
                   input_buffer.insert(input_buffer_cursor, c);
                   input_buffer_cursor += 1;
                   made_change = true;
               },
               KeyCode::Left => {
                   if input_buffer_cursor > 0 {
                       input_buffer_cursor -= 1;
                   }
                   moved_cursor = true;
               },
               KeyCode::Right => {
                   if input_buffer_cursor < input_buffer.len() {
                       input_buffer_cursor += 1;
                   }
                   moved_cursor = true;
               },
               KeyCode::PageUp | KeyCode::PageDown | KeyCode::Up | KeyCode::Down => {
                   let dir = if let KeyCode::PageUp | KeyCode::Up = code { 1 } else { -1 };
                   out_sender.send(OutputMessage::UpdateOffset { amount: dir })
                       .or(Err(RunStdinError::WriteError))?;
               },
               KeyCode::Enter => {
                   let as_str = input_buffer_to_string(&input_buffer);
                   if let Some('/') = input_buffer.first() {
                       let mut split_inp = as_str[1..].split(' ');
                       if let Some(cmd) = split_inp.next() {
                           match cmd {
                               "q" | "quit" => {
                                   writer_sender.send(None).or(Err(RunStdinError::WriteError))?;
                                   return out_sender.send(OutputMessage::Shutdown).or(Err(RunStdinError::OutputError));
                               },
                               "n" | "name" => if let Some(new_name) = split_inp.next() { name = new_name.into(); },
                               _ => {},
                           }
                       }
                   }
                   else {
                       out_sender.send(OutputMessage::UserMessage { data: UserMessage { name: name.clone(), msg: as_str.clone() } }).or(Err(RunStdinError::OutputError))?;
                       writer_sender.send(Some(format!("{}:{}", name, as_str))).or(Err(RunStdinError::WriteError))?;
                   }
                   input_buffer.clear();
                   input_buffer_cursor = 0;
                   made_change = true;
               },
               _ => {},
           }
           if made_change {
               out_sender.send(OutputMessage::UpdateInput { inp: input_buffer_to_string(&input_buffer) }).or(Err(RunStdinError::WriteError))?;
           }
           if made_change || moved_cursor {
               out_sender.send(OutputMessage::UpdateInputCursor { pos: input_buffer_cursor }).or(Err(RunStdinError::WriteError))?;
           }
       }
       else if let Event::Resize(w, h) = event {
           out_sender.send(OutputMessage::Resize { w, h }).unwrap();
       }
   }
   Ok(())
}

enum RunStdoutError {
    CrossTermError { error: Box<crossterm::ErrorKind> },
    StdoutError,
}
fn map_cterr<A>(res: Result<A, crossterm::ErrorKind>) -> Result<A, RunStdoutError> {
    res.map_err(|e| RunStdoutError::CrossTermError { error: Box::new(e) } )
}

enum StdoutMessage {
    UserMessage { msg: UserMessage },
    BasicMessage { msg: String },
}

impl StdoutMessage {
    fn to_string(&self) -> String {
        match self {
            StdoutMessage::UserMessage { msg } => msg.to_string(),
            StdoutMessage::BasicMessage { msg } => msg.clone(),
        }
    }
}

struct OutputState {
    size_w: u16,
    size_h: u16,
    message_list: Vec<StdoutMessage>,
    current_user_input: (String, Vec<String>),
    input_height: usize,
    output_offset: usize,
    new_message_notice: bool,
}
const NEW_MESSAGE_NOTICE: &'static str = "NEW MESSAGE(S) AT BOTTOM";

impl OutputState {
    fn update_output(&mut self) -> Result<(), RunStdoutError> {
        if self.size_h as usize <= self.input_height + 1 { return Ok(()); }
        let remaining_height = self.size_h as usize - self.input_height - 1;
        let mut lines = take_with_height(&self.message_list, self.size_w as usize, self.output_offset, remaining_height);
        if self.new_message_notice {
            let remaining_width = self.size_w as isize - NEW_MESSAGE_NOTICE.len() as isize;
            let line = if remaining_width > 0 {
                let mut line = String::new();
                for _ in 0..remaining_width {
                    line.push('=');
                }
                line.push_str(NEW_MESSAGE_NOTICE);
                line
                //format!("{}{}", [0..remaining_width].iter().map(|_| ' ').collect::<String>(), NEW_MESSAGE_NOTICE) // todo: find out why this line doesnt do the above 6 lines
            }
            else {
                NEW_MESSAGE_NOTICE.chars().take(self.size_w as usize).collect()
            };
            let len = lines.len();
            if len == 0 {
                lines.push(line);
            }
            else {
                lines[len - 1] = line;
            }
        }
        for _ in 0..(remaining_height - lines.len()) {
            lines.insert(0, "*".into());
        }
        let mut stdout = stdout();
        stdout.execute(cursor::MoveTo(0, 0)).unwrap();
        for line in lines {
            map_cterr(stdout.execute(terminal::Clear(ClearType::CurrentLine)))?;
            write!(stdout, "{}", line).or(Err(RunStdoutError::StdoutError))?;
            map_cterr(stdout.execute(cursor::MoveToNextLine(1)))?;
        }
        Ok(())
    }
    fn update_cursor(&mut self, mut pos: usize) -> Result<(), RunStdoutError> {
        let mut y = self.size_h - self.input_height as u16 - 1;
        for line in self.current_user_input.1.iter().take(self.current_user_input.1.len() - 1) {
           if pos < line.len() {
               break;
           }
           else {
               pos -= line.len();
           }
           y += 1;
        }
        map_cterr(stdout().execute(cursor::MoveTo(pos as u16, y)))?;
        Ok(())
    }
    fn update_input(&mut self, inp: String) -> Result<(), RunStdoutError> {
        let wrapped_input = wrap_string(&inp, self.size_w as usize);
        if wrapped_input.len() != self.input_height {
           self.input_height = wrapped_input.len();
           self.update_output()?;
        }
        // write input
        let mut stdout = stdout();
        map_cterr(stdout.execute(cursor::MoveTo(0, (self.size_h as usize - self.input_height - 1) as u16)))?;
        for line in &wrapped_input {
           map_cterr(stdout.execute(terminal::Clear(ClearType::CurrentLine)))?;
           write!(stdout, "{}", line).or(Err(RunStdoutError::StdoutError))?;
           map_cterr(stdout.execute(cursor::MoveToNextLine(1)))?;
        }
        self.current_user_input = (inp, wrapped_input);
        Ok(())
    }
    fn message(&mut self, msg: StdoutMessage) {
        self.message_list.push(msg);
        if self.output_offset != 0 {
            self.new_message_notice = true;
        }
    }
}

fn run_stdout(out_receiver: Receiver<OutputMessage>) -> Result<(), RunStdoutError> {
    let (size_w, size_h) = map_cterr(terminal::size())?;
    let mut state = OutputState {
        size_w, size_h,
        message_list: vec!(),
        current_user_input: (String::new(), vec!()),
        input_height: 1,
        output_offset: 0,
        new_message_notice: false,
    };

    while let Ok(msg) = out_receiver.recv() {
       match msg {
           OutputMessage::UpdateInput { inp } => {
               state.update_input(inp)?;
           },
           OutputMessage::NormalOut { msg } => {
               state.message(StdoutMessage::BasicMessage { msg });
               state.update_output()?;
           },
           OutputMessage::UserMessage { data } => {
               state.message(StdoutMessage::UserMessage { msg: data });
               state.update_output()?;
           },
           OutputMessage::UpdateInputCursor { pos } => {
               state.update_cursor(pos)?;
           },
           OutputMessage::UpdateOffset { amount } => {
               if amount < 0 {
                   if (-amount) as usize > state.output_offset {
                       state.output_offset = 0;
                   }
                   else {
                       state.output_offset -= (-amount) as usize;
                   }
               }
               else {
                   state.output_offset += amount as usize;
               }
               if state.output_offset == 0 {
                   state.new_message_notice = false;
               }
               state.update_output()?;
           },
           OutputMessage::Resize { w, h } => {
               state.size_w = w;
               state.size_h = h;
               state.update_output()?;
               let user_line = state.current_user_input.0.clone();
               state.update_input(user_line)?;
           },
           OutputMessage::Shutdown => break,
       }
       stdout().flush().or(Err(RunStdoutError::StdoutError))?;
    }
    Ok(())
}

enum RunReadStreamError {
    Output,
    StreamWriter,
    Conversion,
}

fn run_read_stream(stream: Arc<TcpStream>, out_sender: Sender<OutputMessage>, writer_sender: Sender<Option<String>>, shared_secret: Arc<Mutex<SharedSecret>>) -> Result<(), RunReadStreamError>{
    let mut reader = BufReader::new(&*stream);
    loop {
        let mut bytes = vec!();
        match reader.read_until(0, &mut bytes) {
            Ok(_) => {}, Err(e) => {
                out_sender.send(OutputMessage::NormalOut { msg: e.to_string() }).or(Err(RunReadStreamError::Output))?;
                continue;
            },
        }
        if bytes.is_empty() {
            // this should mean that the stream has closed
            writer_sender.send(None).or(Err(RunReadStreamError::StreamWriter))?;
            break;
        }
        bytes.pop(); // pop the zero off at the end
        let stringed_bytes = match String::from_utf8(bytes) {
            Ok(bytes) => bytes,
            Err(e) => {
                out_sender.send(OutputMessage::NormalOut { msg: e.to_string() }).or(Err(RunReadStreamError::Output))?;
                continue;
            },
        };
        let str_bytes = stringed_bytes.as_str();
        let decoded_string = decode(str_bytes);
        let decoded_string = decoded_string.as_slice();
        let nonce = decoded_string[0..24].try_into().or(Err(RunReadStreamError::Conversion))?;
        let decrypted = match decrypt(shared_secret.lock().unwrap().as_bytes(), nonce, decoded_string[24..].as_ref()) {
            Ok(val) => val,
            Err(e) => {
                out_sender.send(OutputMessage::NormalOut { msg: format!("failed to decrypt: {}", e.to_string()) }).or(Err(RunReadStreamError::Output))?;
                continue;
            },
        };
        let plaintext = match String::from_utf8(decrypted) {
            Ok(val) => val,
            Err(e) => {
                out_sender.send(OutputMessage::NormalOut { msg: format!("failed to decode from utf8: {}", e.to_string()) }).or(Err(RunReadStreamError::Output))?;
                continue;
            }
        };
        let mut colonsplit = plaintext.split(':').fuse();
        let (name, msg) = (colonsplit.next().unwrap_or(""), colonsplit.next().unwrap_or(""));
        out_sender.send(OutputMessage::UserMessage { data: UserMessage { name: name.into(), msg: msg.into() } }).or(Err(RunReadStreamError::Output))?;
    }
    Ok(())
}

enum RunWriteStreamError {
    Encryption,
    StreamWrite,
    Output,
    Shutdown { error: Box<io::Error> },
}

fn run_write_stream(stream: Arc<TcpStream>, writer_receiver: Receiver<Option<String>>, out_sender: Sender<OutputMessage>, shared_secret: Arc<Mutex<SharedSecret>>, rng: &mut OsRng) -> Result<(), RunWriteStreamError> {
    let mut writer = BufWriter::new(&*stream);
    while let Ok(msg) = writer_receiver.recv() {
        match msg {
            Some(text) => {
                let (nonce, data) = encrypt(rng, shared_secret.lock().unwrap().as_bytes(), &text.as_bytes()[..]).or(Err(RunWriteStreamError::Encryption))?;
                let encoded = encode([&nonce[..], data.as_slice()].concat().as_slice());
                let mut bytes = encoded.into_bytes();
                bytes.push(0);
                writer.write_all(bytes.as_slice()).or(Err(RunWriteStreamError::StreamWrite))?;
                match writer.flush() {
                    Ok(()) => {},
                    Err(e) => {
                        out_sender.send(OutputMessage::NormalOut { msg: format!("failed to flush: {}", e.to_string()) }).or(Err(RunWriteStreamError::Output))?;
                        continue;
                    },
                }
            },
            None => {
                out_sender.send(OutputMessage::NormalOut { msg: "connection shut down".into() }).or(Err(RunWriteStreamError::Output))?;
                match stream.shutdown(Shutdown::Both) {
                    Ok(()) => break, Err(e) => return Err(RunWriteStreamError::Shutdown { error: Box::new(e) }),
                }
            },
        }
    }
    Ok(())
}

fn wrap_string(data: &String, width: usize) -> Vec<String> {
    let mut words = data.split(' ').map(|s| String::from(s)).collect::<Vec<String>>();
    let mut lines = vec!();
    loop {
        let mut current_string = String::new();
        while current_string.len() <= width {
            if words.len() == 0 { break; }
            let word = words.remove(0);
            let remaining_space = width - current_string.len();
            if remaining_space < word.len() {
                if current_string.is_empty() {
                    // fit what we can
                    let partial_word = word.chars().take(width).collect::<String>();
                    current_string = partial_word;
                    words.insert(0, word.chars().skip(width).collect());
                }
                else {
                    words.insert(0, word); // put it back so we can put it on the next line
                }
                break;
            }
            else {
                current_string.push_str(word.as_str());
                if word.len() < remaining_space {
                    current_string.push(' ');
                }
            }
        }
        lines.push(current_string);
        if words.len() == 0 { break; }
    }
    lines
}

fn take_with_height(msgs: &Vec<StdoutMessage>, width: usize, offset: usize, height: usize) -> Vec<String> {
    let mut offset = offset;
    let mut lines = vec!();
    for msg in msgs.iter().rev() {
        if lines.len() < height {
            let mut msg_lines = wrap_string(&msg.to_string(), width);
            for line in msg_lines.iter_mut().rev() {
                if lines.len() < height {
                    if offset > 0 {
                        offset -= 1;
                    }
                    else {
                        lines.insert(0, line.clone());
                    }
                }
                else {
                    break;
                }
            }
        }
        else {
            break;
        }
    }
    lines
}

fn exchange(stream: &mut TcpStream, rng: &mut OsRng, preshared_public: SneakKey, preshared_private: SneakKey) -> Result<SharedSecret, io::Error> {
    // set up preshared
    let preshared_private = StaticSecret::from(preshared_private);
    let preshared_public = PublicKey::from(preshared_public);
    let preshared_key = preshared_private.diffie_hellman(&preshared_public);
    // generate key
    let mut rng_cl = rng.clone();
    let private_key = EphemeralSecret::new(rng);
    let public_key = PublicKey::from(&private_key);
    // send public key
    // - encrypt it with preshared
    let (nonce, encrypted) = encrypt(&mut rng_cl, preshared_key.as_bytes(), public_key.as_bytes()).expect("encryption error");
    let ciphertext_bytes = [&nonce[..], encrypted.as_slice()].concat();
    let encoded = encode(ciphertext_bytes.as_slice());
    let mut bytes = encoded.into_bytes();
    bytes.push(0);
    stream.write_all(bytes.as_slice()).expect("failed to write to stream");
    stream.flush().expect("failed to flush stream");
    // receive their public key
    let mut reader = BufReader::new(stream);
    let mut bytes = vec!();
    reader.read_until(0, &mut bytes)?;
    bytes.pop(); // need to pop off last byte, which should be a zero byte that we don't want
    let stringed_bytes = match String::from_utf8(bytes) {
        Ok(bytes) => bytes,
        Err(e) => panic!("error converting bytes to string: {}", e),
    };
    let decoded_string = decode(stringed_bytes.as_str());
    let nonce: [u8; 24] = decoded_string.as_slice()[0..24].try_into().expect("failed to convert byte slice into nonce");
    let ciphertext = &decoded_string.as_slice()[24..];
    let decrypted = decrypt(preshared_key.as_bytes(), nonce, ciphertext).expect("failed to decrypt");
    let key_bytes: SneakKey = decrypted.as_slice().try_into().expect("failed to convert decoded string to key bytes");
    let their_public_key = PublicKey::from(key_bytes);
    // return shared key
    Ok(private_key.diffie_hellman(&their_public_key))
}

fn connect_client(addr: SocketAddr) -> Result<TcpStream, io::Error> {
    println!("attempting connection to {}", addr.to_string());
    TcpStream::connect_timeout(&addr, Duration::new(1000, 0))
}

fn connect_server(addr: SocketAddr) -> Result<TcpStream, io::Error> {
    println!("attempting to bind to {}", addr.to_string());
    let listener = TcpListener::bind(addr)?;
    println!("waiting for connection on {}", addr.to_string());
    listener.incoming().next().unwrap()
}

